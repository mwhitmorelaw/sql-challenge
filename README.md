# Employee Database

This project provides employee data in the form of CSV files. The CSV data can be imported into a PostgreSQL database using the included SQL scripts to create and table structure. The data can then be analyzed using the queries in the queries.sql file. 

## Getting Started

### SQL Queries

In order to create the tables needed to analyze the employee data, you will need to run the tables.sql file against a PostgreSQL server. This can be done in PgAdmin. Once the tables are created the CSV files in EmployeeSQL can be imported into the respective tables. These files have headers, so this must be accounted for when importing, also, some tables, have additional columns that are used as primary keys. These columns are not in the CSV files and thus should be ignored when importing. 

Once the data has been ignored, the queries in querie.sql can be individually ran to analyize the data.  

### Visualizations
To view the visualizations provided in the bonus.ipynb, from the project root run the following command line statement: 'jupyter-notebook.exe bonus.ipynb'.
Once Jupyter opens, 
In order to run the notebook, you will need to create a file in named secrets.py. 
This file needs to store one variable, 'postgres_pw' (which will store your PostgreSQL server password).
Once a notebook is open in Jupyter, select the first cell and press SHIFT + ENTER to run it.
This will also move the cell selection to the next cell down. The remainder of the notebook results
can be viewed by continuing to press SHIFT + ENTER in each cell.

### Prerequisites

This project requires PostgresSQL server and client. Installation instructions can be found [here](https://www.postgresql.org/docs/9.3/tutorial-install.html). 
This project requires Jupyter Notebook to be installed. Installation instructions can be found [here](https://jupyter.org/install).

### Installing

This project relies on the following python dependencies:
- pandas==0.23.4
- matplotlib==3.0.2
- numpy==1.15.4

These packages can be installed via pip or Anaconda

## Deployment

The scripts included in this project are self-contained and intended to be used for one-off runs. Assuming you feel these scripts should be deployed to production, no additional steps are needed beyond those stated in 'Getting Started' above.

## Built With

* [PostgreSQL](https://www.postgresql.org) - Relational Database Management System
* [Python](http://www.python.org) - Language used
* [Jupyter](https://jupyter.org) - IDE used
* [Pandas](https://pandas.pydata.org) - Data Manipulation Library
* [Matplotlib](https://matplotlib.org) - Data Visualization Library
* [Numpy](https://numpy.org) - Data Manipulation Library

## Versioning

We use [SemVer](http://semver.org/) for versioning. The version is currently 1.0.0.

## Authors

* **Michael Whitmore** - *All work* - [mwhitmorelaw](https://gitlab.com/mwhitmorelaw)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Guido van Rossum
