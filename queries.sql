CREATE OR REPLACE VIEW dept_union AS
SELECT 
	dm.emp_no, 
	dm.to_date,
	(SELECT d.dept_name
	 FROM department AS d 
	 WHERE d.dept_no = dm.dept_no) as dept_name,
	dm.dept_no,
	'Manager' AS employee_type
FROM dept_manager AS dm
UNION
SELECT 
	de.emp_no, 
	de.to_date,
	(SELECT d.dept_name
	 FROM department AS d 
	 WHERE d.dept_no = de.dept_no) as dept_name,
	de.dept_no,
	'Employee' AS employee_type
FROM dept_emp AS de;


-- List the following details of each employee: 
-- employee number, last name, first name, gender, and salary.
SELECT 
	e.emp_no AS "Employee Number",
	e.last_name AS "Last Name",
	e.first_name AS "First Name",
	(CASE WHEN e.gender = 'M' THEN 'Male' ELSE 'Female' END) AS "Gender",
	s.salary AS "Salary",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e 
JOIN salaries AS s 
	ON s.emp_no = e.emp_no
JOIN dept_union AS d 
	ON e.emp_no = d.emp_no
;

-- List employees who were hired in 1986.
SELECT 
	e.emp_no AS "Employee Number",
	e.last_name AS "Last Name",
	e.first_name AS "First Name",
	(CASE WHEN e.gender = 'M' THEN 'Male' ELSE 'Female' END) AS "Gender",
	e.hire_date AS "Hire Date",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e
JOIN dept_union AS d 
	ON e.emp_no = d.emp_no
WHERE date_part('year', e.hire_date) = 1986
;

-- List the manager of each department with the following information: 
-- department number, department name, the manager's employee number, 
-- last name, first name, and start and end employment dates.

-- end employment date should be the latest time a give manager worked 
-- either as a manager or as an employee
-- employee department will either be on the dept_manager or dept_emp table

--Find the last day each employee was working.
CREATE OR REPLACE VIEW employee_tenure_raw AS
SELECT 
	emp_no,
	dept_no,
	dept_name,
	MAX(to_date) OVER (PARTITION BY emp_no) AS last_day,
	to_date
FROM dept_union
;

-- Clean up formatting
CREATE OR REPLACE VIEW employee_tenure AS
SELECT 
	r.emp_no, 
	e.first_name,
	e.last_name,
	e.hire_date,
	r.dept_name AS most_recent_dept_name, 
	r.dept_no AS most_recent_dept_no,
	CASE 
		WHEN date_part('year', r.last_day) = '9999' 
		THEN 'Current Employee' 
		ELSE CAST(r.last_day AS TEXT) 
	END
FROM employee_tenure_raw AS r
JOIN employees AS e
	ON (r.emp_no = e.emp_no)
WHERE r.last_day = r.to_date;

SELECT
	t.most_recent_dept_no AS "Department Number",
	t.most_recent_dept_name AS "Department Name",
	t.emp_no AS "Employee Number",
	t.last_name AS "Last Name",
	t.first_name AS "First Name",
	t.hire_date AS "Employment Start Date",
	t.last_day AS "Employment End Date"
-- We only want to look at the managers, but we need to get there
-- employment dates from employee_tenure
FROM dept_manager as m
JOIN employee_tenure AS t 
	ON m.emp_no = t.emp_no
;

-- List the department of each employee with the following information: 
-- employee number, last name, first name, and department name.
SELECT 	
	e.emp_no AS "Employee Number",
	e.last_name AS "Last Name",
	e.first_name AS "First Name",
	d.dept_name AS "Department Name",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e
JOIN dept_union AS d
	ON d.emp_no = e.emp_no
;

-- List all employees whose first name is "Hercules" and last names begin with "B."
SELECT
	*
FROM employees
WHERE first_name = 'Hercules' 
AND last_name LIKE 'B%'
;

-- List all employees in the Sales department, including their 
-- employee number, last name, first name, and department name.
SELECT 	
	e.emp_no AS "Employee Number",
	e.last_name AS "Last Name",
	e.first_name AS "First Name",
	d.dept_name AS "Department Name",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e
JOIN dept_union AS d
	ON d.emp_no = e.emp_no
WHERE d.dept_name = 'Sales'
;

-- List all employees in the Sales and Development departments, 
-- including their employee number, last name, first name, and department name.
SELECT 	
	e.emp_no AS "Employee Number",
	e.last_name AS "Last Name",
	e.first_name AS "First Name",
	d.dept_name AS "Department Name",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e
JOIN dept_union AS d
	ON d.emp_no = e.emp_no
WHERE d.dept_name IN ('Sales', 'Development')
;

-- In descending order, list the frequency count of employee last names, i.e., how many employees share each last name.
SELECT 
	e.last_name AS "Last Name", 
	COUNT(e.emp_no) AS "Name Count",
	-- 'employee' is an ambiguous term as it is not clear if it includes managers.
	-- 'Employment Type' delimiter will be included to reduce ambiguity
	d.employee_type AS "Employee Type"
FROM employees AS e
JOIN dept_union AS d
	ON d.emp_no = e.emp_no
GROUP BY last_name, d.employee_type
ORDER BY COUNT(e.emp_no) DESC
;
